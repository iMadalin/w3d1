#include <iostream>

unsigned int modulus(int a)
{
    std::cout << __FUNCSIG__ << std::endl;
    return (a < 0) ? -a : a;
}

double modulus(double a)
{
    std::cout << __FUNCSIG__ << std::endl;
    return (a < 0) ? -a : a;
}

int main()
{
    float a = 7.0;
    std::cout << modulus(a) << std::endl;
    return 0;
}
