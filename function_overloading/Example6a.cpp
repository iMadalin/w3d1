#include <iostream>

unsigned int modulus(int a)
{
    std::cout << __FUNCSIG__ << std::endl;
    return (a < 0) ? -a : a;
}

unsigned long modulus(long long a)
{
    std::cout << __FUNCSIG__ << std::endl;
    return (a < 0) ? -a : a;
}

int main()
{
    char a = 7;
    std::cout << modulus(a) << std::endl;
    return 0;
}
