#include <iostream>

unsigned int modulus(int& a)
{
    std::cout << __FUNCSIG__ << std::endl;
    return (a < 0) ? -a : a;
}

unsigned int modulus(const int& a)
{
    std::cout << __FUNCSIG__ << std::endl;
    return (a < 0) ? -a : a;
}

int main() {
    int a = -5;
    const int b = 3;
    std::cout << modulus(a) << std::endl;
    std::cout << modulus(b) << std::endl;
    return 0;
}