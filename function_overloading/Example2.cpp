#include <iostream>

double modulus(double a)
{
    std::cout << __FUNCSIG__ << std::endl;
    return (a < 0) ? -a : a;
}

unsigned int modulus(const int a)
{
    std::cout << __FUNCSIG__ << std::endl;
    return (a < 0) ? -a : a;
}

int main()
{
    int a = -5;
    std::cout << modulus(a) << std::endl;
    return 0;
}
