#include <iostream>

unsigned int modulus(int a)
{
    std::cout << __FUNCSIG__ << std::endl;
    return (a < 0) ? -a : a;
}

double modulus(double a)
{
    std::cout << __FUNCSIG__ << std::endl;
    return (a < 0) ? -a : a;
}

int main()
{
    double a = 2.35;
    int b = 3;
    std::cout << modulus(a) << std::endl;
    std::cout << modulus(b) << std::endl;
    return 0;
}
