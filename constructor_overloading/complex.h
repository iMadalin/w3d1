#pragma once

// put your declarations here
class Complex {
    double _r, _i;
public:
    double Real();
    double Imaginary();
};
